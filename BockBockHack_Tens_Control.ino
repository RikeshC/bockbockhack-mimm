int incomingByte = 0; // for incoming serial data
int currentdata = 0;
int lastdata = 0;
#define datadelay 100
#define tenspin 3
#define duration 100
#define frequency 50

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
}

void loop() {
  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    // say what you got:
    //Serial.print("I received: ");
    //Serial.println(incomingByte, DEC);
    currentdata = millis();
    if ((currentdata-lastdata) > datadelay ) {
      tone(tenspin, frequency, duration);
    }

    lastdata = currentdata;
  }
}
